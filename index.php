<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Sistem Hercules Fitness</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="aset/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="aset/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="aset/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
  </head>
  <body style="overflow-y: hidden; background-color:#e3e3e3; " class="login-page">
    <div class="login-box">
      <div class="login-logo">
      </div>
      <div class="login-box-body"  style="background-color:#e3e3e3; border-radius: 50px;">
        <a href="index.php"><b style="color:#404040; font-size: 60px; font-family: Geneva;"><center>admin</center></b></a>
        <center><img src="images/lg3.jpeg" style="width: 150px;"></center>
        <form name="login-form" action="cek_login.php"  class="login-form" method="post">
          <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" placeholder="Masukan Username Anda" style="border-radius: 50px;  font-family: Geneva;">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback"> 
            <input name="password" type="password" class="form-control" placeholder="Masukan Password Anda" style="border-radius: 50px; font-family: Geneva;">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
                <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat" style="width: 370%; border-radius: 15px;">Masuk Sekarang</button>
            </div><!-- /.col -->
          </div>
        </form>         
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
  <!-- jQuery 2.1.4 -->
    <script src="aset/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="aset/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="aset/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>