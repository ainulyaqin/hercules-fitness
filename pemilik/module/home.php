<?php
$namaBln = array("1" => "Januari", "2" => "Februari", "3" => "Maret", "4" => "April", "5" => "Mei", "6" => "Juni", 
					 "7" => "Juli", "8" => "Agustus", "9" => "September", "10" => "Oktober", "11" => "November", "12" => "Desember");
include ("../inc/koneksi.php"); 
include ("../inc/fungsi_hdt");  ?>

<br/>
<div style="margin-right:10%;margin-left:15%" class="alert alert-info alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<p><i class="icon fa fa-info"></i>
Welcome <?php echo $_SESSION['nama']; ?>! &nbsp;&nbsp;
Anda berada di halaman "Pemilik"
</p>
</div>
 
<script src="module/js/highcharts.js"></script>
<script src="module/js/exporting.js"></script>
<div class="row">
<div class="col-md-12">
<div class="box box-info">
<div class="box-header with-border">
  <h3 class="box-title">Penjualan Terbaik</h3>
  <div class="box-tools pull-right">
  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
<script type="text/javascript">
  var chart1; // globally available
$(document).ready(function() {
      chart1 = new Highcharts.Chart({
         chart: {
            renderTo: 'container',
            type: 'column'
         },   
         title: {
            text: 'Grafik Penjualan Terbaik'
         },
         subtitle: {
                text: 'Hercules Fitness',
                x: -20
            },
         xAxis: {
            categories: ['Nama Barang']
         },
         yAxis: {
            title: {
               text: 'Jumlah Pembelian'
            }
         },
              series:             
            [
            <?php 
          include('config.php');
           $sql   = "SELECT a.nama_barang FROM tb_barang a join tb_detailpenjualan b on a.id_barang=b.id_barang group by a.nama_barang";
            $query = mysql_query( $sql ) or die(mysql_error());
            while( $ret = mysql_fetch_array( $query ) ){
              $nama=$ret['nama_barang'];                     
                 $sql_jumlah   = "SELECT tb_barang.`nama_barang`,SUM(jumlah) AS terbaik FROM tb_detailpenjualan JOIN tb_barang ON tb_detailpenjualan.`id_barang`= tb_barang.`id_barang` WHERE tb_barang.nama_barang='$nama' AND tb_barang.id_barang=tb_detailpenjualan.id_barang GROUP BY nama_barang";        
                 $query_jumlah = mysql_query( $sql_jumlah ) or die(mysql_error());
                 while( $data = mysql_fetch_array( $query_jumlah ) ){
                    $jumlah = $data['terbaik'];                 
                  }             
                  ?>
                  {
                      name: '<?php echo $nama; ?>',
                      data: [<?php echo $jumlah; ?>]
                  },
                  <?php } ?>
            ]
      });
   });  
</script>
<div id='container'></div>    
</div></div>
</div>
<div class="col-md-12">
<div class="box box-info">
<div class="box-header with-border">
  <h3 class="box-title">Promosi Berhasil</h3>
  <div class="box-tools pull-right">
  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
<script type="text/javascript">
    //2)script untuk membuat grafik, perhatikan setiap komentar agar paham
$(function () {
    var chart;
    $(document).ready(function() {
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'yyy', //letakan grafik di div id container
        //Type grafik, anda bisa ganti menjadi area,bar,column dan bar
                type: 'line',  
                marginRight: 130,
                marginBottom: 25
            },
            title: {
                text: 'Promosi Berhasil',
                x: -20 //center
            },
            subtitle: {
                text: 'Hercules Fitness',
                x: -20
            },
            xAxis: { //X axis menampilkan data tahun 
                categories: [<?php        
        $sql = mysql_query(" SELECT MONTH(tgl_masuk) AS bln FROM tb_program GROUP BY MONTH(tgl_masuk)");
while ($k = mysql_fetch_array($sql)) { 
        echo "'";echo $namaBln[$k['bln']];echo "', ";
        }?> ]
            },
            yAxis: {
                title: {  //label yAxis
                    text: 'Jumlah Promosi Program Berhasil'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080' //warna dari grafik line
                }]
            },
            tooltip: { 
      //fungsi tooltip, ini opsional, kegunaan dari fungsi ini 
      //akan menampikan data di titik tertentu di grafik saat mouseover
                formatter: function() {
                        return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y ;
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -10,
                y: 100,
                borderWidth: 0
            },
      //series adalah data yang akan dibuatkan grafiknya,
      //saat ini mungkin anda heran, buat apa label indonesia dikanan 
      //grafik, namun fungsi label ini sangat bermanfaat jika
      //kita menggambarkan dua atau lebih grafik dalam satu chart,
      //hah, emang bisa? ya jelas bisa dong, lihat tutorial selanjutnya 
            series: [{  
                name: 'Pelanggan Program',  
                name: 'Pelanggan Program',  
        //data yang akan ditampilkan 
                data: [<?php        
        $sq = mysql_query("SELECT COUNT(tb_program.id_program) AS id_program, MONTH(tb_program.`tgl_masuk`)AS bln FROM tb_detailprogram JOIN tb_program ON tb_detailprogram.`id_program`=tb_program.`id_program` GROUP BY bln ");
while ($q = mysql_fetch_array($sq)) { 
        echo $q['id_program'];echo ", ";
        }?>]
            }]
        });
    });
    
});
    </script>
<div id="yyy" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
</div></div>
</div>

<?php
 $sql = "SELECT jk, COUNT( jk ) AS Total FROM  tb_program GROUP BY jk"; 
 $hasil = mysql_query($sql);
?>
     
<div class="row">
<div class="col-md-12">
<div class="box box-info">
<div class="box-header with-border">
  <h3 class="box-title">Data Pelanggan</h3>
  <div class="box-tools pull-right">
  <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
  <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
  </div>
</div>
<div class="box-body">
  <script type="text/javascript">
       $(function () {
       
    $('#bola').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,//null,
            plotShadow: false,
        },
        title: {
            text: 'Ratio Jenis Kelamin Pelanggan Dengan Program Latihan'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Hercules Fitness',
            data: [
      <?php     
      while($data=mysql_fetch_array($hasil))
      { ?>
                ['<?php echo $data['jk']?>',   <?php echo $data['Total']?>],               
       <?php
       }//end while
       ?>
            ]
        }]
    });
});   
    </script>
   <div id="bola" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
</div> 
</div></div>

