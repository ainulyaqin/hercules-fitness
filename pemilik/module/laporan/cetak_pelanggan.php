<?php 
include "head.php";
?>
          <section class="content-header">
            <h1>
             Laporan
              <small>Data Pelanggan</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
              <li class="active">Data Pelanggan</li>
            </ol>
          </section>

           
          <section class="content">
            <div class="text-center">
			<h3><img src="../../../images/lg2.jpeg" style="width: 200px;"/></h3>
			<b>Jalan H.Abdul Kahar, Kec. Gamping, Kabupten Sleman, <br/>
			Daerah Istimewa Yogyakarta</b>
			</div><br/>
             
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title center">Daftar Pelanggan</h3>
				<span class="pull-right">
				Yogyakarta, <?php echo Indonesia2Tgl(date('Y-m-d'));?> 
				</span>					
              </div>
              <div class="box-body">
<table class="table table-bordered table-striped">
<thead>
<tr class="text-red">
		<th class="col-sm-1">No</th> 
    <th class="col-sm-2">Nama Pelanggan</th>
    <th class="col-sm-1">Jenis Kelamin</th>
    <th class="col-sm-1">Usia Pelanggan</th>
    <th class="col-sm-1">BB Sekarang (Kg)</th>
    <th class="col-sm-1">BB Target (Kg)</th>
    <th class="col-sm-1">Jenis Latihan</th>
    <th class="col-sm-1">Tanggal Latihan Awal</th>
    <th class="col-sm-1">Tanggal Latihan Akhir</th>
    <th class="col-sm-1">Total Bulan</th> 
    <th class="col-sm-2">Keterangan</th> 
	</tr>
</thead>

<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT tb_pelanggan.`nama_pelanggan`, tb_program.`id_program`, jk,usia, bbskrg, bbtarget, jenis_latihan,`tgl_masuk`,tgl_selesai, setatus AS keterangan,totalbulan FROM tb_program
 JOIN tb_pelanggan ON tb_program.`id_pelanggan` = tb_pelanggan.id_pelanggan
 -- JOIN tb_detailprogram ON tb_detailprogram.`id_program` = tb_program.`id_program`
";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 
$Kode = $tampilkan['id_program'];
$jenis_latihan = $tampil ['jenis_latihan']; 
?>

	<tr>
	<td><?php echo $no++; ?></td> 
  <td><?php echo $tampilkan['nama_pelanggan']; ?></td>
  <td><?php echo $tampilkan['jk']; ?></td>
  <td><?php echo $tampilkan['usia']; ?></td>
  <td><?php echo $tampilkan['bbskrg']; ?></td>
  <td><?php echo $tampilkan['bbtarget']; ?></td>
  <td><?php echo $tampilkan['jenis_latihan']; ?></td>
  <td><?php echo $tampilkan['tgl_masuk']; ?></td>
  <td><?php echo $tampilkan['tgl_selesai']; ?></td>
  <td><?php echo $tampilkan['totalbulan']; ?></td>
  <td><?php echo $tampilkan['keterangan']; ?></td>
<?php
}
?>
	</tr>
			</tbody>
		</table>	
              </div><!-- /.box-body -->
            </div>
          </section><!-- /.content -->
<?php
include "tail.php";
?>