<?php 
include "head.php";
?>
          <section class="content-header">
            <h1>
             Laporan
              <small>Data Penjualan</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
              <li class="active">Data Penjualan</li>
            </ol>
          </section>

           
          <section class="content">
            <div class="text-center">
			<h3><img src="../../../images/lg2.jpeg" style="width: 200px;"/></h3>
			<b>Jalan H.Abdul Kahar, Kec. Gamping, Kabupten Sleman, <br/>
			Daerah Istimewa Yogyakarta</b>
			</div><br/>
             
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title center">Daftar Penjualan</h3>
				<span class="pull-right">				
				Yogyakarta, <?php echo Indonesia2Tgl(date('Y-m-d'));?> 
				</span>					
              </div>
              <div class="box-body">
                <table  class="table table-bordered table-striped">
<thead>
<tr class="text-blue">
     <th class="col-sm-1">No</th>   
     <th class="col-sm-2">ID Penjualan</th>
         <th class="col-sm-2">ID Barang</th>
         <th class="col-sm-2">Nama Barang</th>
         <th class="col-sm-2">Tanggal Penjualan</th>
         <th class="col-sm-2">Harga Per Barang</th> 
         <th class="col-sm-2">Jumlah Beli</th>
         <th class="col-sm-2">Total</th>     
  </tr>
</thead>

<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT tb_detailpenjualan.`id_penjualan`, tb_detailpenjualan.`id_barang`, tb_penjualan.`tgl_penjualan`,tb_barang.`nama_barang`,tb_detailpenjualan.`jumlah`, tb_detailpenjualan.jumlah*tb_barang.harga AS total,tb_barang.`harga`
  FROM tb_detailpenjualan 
  JOIN tb_barang ON tb_detailpenjualan.`id_barang` = tb_barang.`id_barang`
  JOIN tb_penjualan ON tb_detailpenjualan.`id_penjualan`= tb_penjualan.`id_penjualan`";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 
?>

	<tr>	
  <td><?php echo $no++; ?></td> 
  <td><?php echo $tampilkan['id_penjualan']; ?></td>
  <td><?php echo $tampilkan['id_barang']; ?></td>
  <td><?php echo $tampilkan['nama_barang']; ?></td>
  <td><?php echo $tampilkan['tgl_penjualan']; ?></td>
  <td>Rp.<?php echo number_format($tampilkan['harga']) ?>,-</td>
  <td> <center><?php echo $tampilkan['jumlah']; ?> </center></td>
  <td>Rp.<?php echo number_format($tampilkan['total']) ?>,-</td>
	<?php
	}
	?>
	</tr>
			</tbody>
		</table>	
              </div><!-- /.box-body -->
            </div>
          </section><!-- /.content -->

<?php
include "tail.php";?>