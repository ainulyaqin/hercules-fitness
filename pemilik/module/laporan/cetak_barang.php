<?php 
include "head.php";
?>
          <section class="content-header">
            <h1>
             Laporan
              <small>Data Barang</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Laporan</a></li>
              <li class="active">Data Barang</li>
            </ol>
          </section>

           
          <section class="content">
            <div class="text-center">
			<h3><img src="../../../images/lg2.jpeg" style="width: 200px;" /></h3>
			<b>Jalan H.Abdul Kahar, Kec. Gamping, Kabupten Sleman, <br/>
			Daerah Istimewa Yogyakarta</b>
			</div><br/>
             
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title center">Daftar Barang</h3>
				<span class="pull-right">
				Yogyakarta, <?php echo Indonesia2Tgl(date('Y-m-d'));?> 
				</span>					
              </div>
              <div class="box-body">
<table class="table table-bordered table-striped">
<thead>
	<tr class="text-red">
		<th class="col-sm-1">No</th>		
		<th class="col-sm-5">Nama Barang</th>
		<th class="col-sm-2">Harga</th> 
		<th class="col-sm-2">Stok</th> 
	</tr>
</thead>

<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT nama_barang, harga, stok FROM tb_barang";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 
?>

	<tr>
	<td><?php echo $no++; ?></td>	
	<td><?php echo $tampilkan['nama_barang']; ?></td>
	<td>Rp.<?php echo number_format($tampilkan['harga']) ?>,-</td>
	<td><?php echo $tampilkan['stok']; ?></td>
<?php
}
?>
	</tr>
			</tbody>
		</table>	
              </div><!-- /.box-body -->
            </div>
          </section><!-- /.content -->
<?php
include "tail.php";
?>