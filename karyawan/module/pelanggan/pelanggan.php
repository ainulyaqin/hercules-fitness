<?php
$aksi="module/pelanggan/pelanggan_aksi.php";
switch($_GET[aksi]){
default:
?>
<!----- ------------------------- MENAMPILKAN DATA MASTER pelanggan ------------------------- ----->      
<center> <div class="batas"> </div></center>
<br/>
<div class="row">
<div class="col-md-12" style="font-family: Geneva;">
  <div class="box box-solid box-success" style="border-radius: 15px;">
    <div class="box-header" style="border-radius: 15px;">
    <h3 class="btn btn disabled box-title" style="font-family: Geneva;">
    <i class="fa fa-plus"></i>
    Tambah Data Pelanggan</h3>      
    </div>    
  <div class="box-body">
  <?php
$sql ="SELECT max(id_pelanggan) as terakhir from tb_pelanggan";
  $hasil = mysql_query($sql);
  $data = mysql_fetch_array($hasil);
  $lastID = $data['terakhir'];
  $lastNoUrut = substr($lastID, 3, 9);
  $nextNoUrut = $lastNoUrut + 1;
  $nextID = "PLG".sprintf("%03s",$nextNoUrut);
?> 
   <form class="form-horizontal" action="<?php echo $aksi?>?module=pelanggan&aksi=tambah" role="form" method="post">             

  <div class="form-group">
    <label class="col-sm-2 control-label">ID Pelanggan</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="id_pelanggan" value="<?php echo  $nextID; ?>" >
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Nama Pelanggan</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="nama_pelanggan" placeholder="Nama Pelanggan" onkeypress="return goodchar(event,'',this)">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Email</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="email" placeholder="Email">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">No Telphone</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="notelp" placeholder="No Telphone" onkeypress="return goodnumber(event,'',this)">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">Alamat</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="alamat" placeholder="Alamat Pelanggan" onkeypress="return goodall(event,'',this)">
    </div>
  </div><div class="form-group">
    <label class="col-sm-4"></label>
    <div class="col-sm-4">
  <hr/>
      <button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i><i> Reset</i></button> 
    </div>
  </div>
</form>
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
<div class="col-md-12">
  <div class="box box-solid box-success" style="border-radius: 15px;">
    <div class="box-header" style="border-radius: 15px;">
    <h3 class="btn disabled box-title" style="color: white; font-family: Geneva;">
    <i class="fa fa-male"></i>
    Data Pelanggan Tersimpan</h3> 
    </div>    
  <div class="box-body" style="font-family: Geneva;">
  <table id="example2" class="table table-bordered table-striped">
<thead>
  <tr class="text-green">
    <th class="col-sm-2">No</th> 
    <th class="col-sm-2">Nama Pelanggan</th>
    <th class="col-sm-2">Email</th>
    <th class="col-sm-2">No Telphone</th> 
    <th class="col-sm-2">Alamat</th> 
    <th class="col-sm-2">AKSI</th>  
  </tr>
</thead>

<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT * FROM tb_pelanggan";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 
$Kode = $tampilkan['id_pelanggan'];
$jenis_latihan = $tampil ['jenis_latihan']; 

?>

  <tr>
  <td><?php echo $no++; ?></td> 
  <td><?php echo $tampilkan['nama_pelanggan']; ?></td> 
  <td><?php echo $tampilkan['email']; ?></td>
  <td><?php echo $tampilkan['notelp']; ?></td>
  <td><?php echo $tampilkan['alamat']; ?></td>
  <td align="center">
  <a class="btn btn-xs btn-info" href="?module=pelanggan&aksi=edit&id_pelanggan=<?php echo $tampilkan['id_pelanggan'];?>" alt="Edit Data"><i class="glyphicon glyphicon-pencil"></i></a>
  <a class="btn btn-xs btn-danger"href="<?php echo $aksi ?>?module=pelanggan&aksi=hapus&id_pelanggan=<?php echo $tampilkan['id_pelanggan'];?>"  alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA <?php echo $Kode; ?>  ?')"> <i class="glyphicon glyphicon-trash"></i></a>
  </td>
  <?php
  }
  ?>
  </tr>
      </tbody>
    </table>
  </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<!----- ------------------------- END TAMBAH DATA MASTER pelanggan ------------------------- ----->
<?php 
break;
case "edit" :
$data=mysql_query("select * from tb_pelanggan where id_pelanggan='$_GET[id_pelanggan]'");
$edit=mysql_fetch_array($data);
?>

<!----- ------------------------- EDIT DATA MASTER pelanggan ------------------------- ----->
<h3 class="box-title margin text-center">Edit Data Pelanggan "<?php echo $_GET['id_pelanggan']; ?>"</h3>
<br/>
<form class="form-horizontal" action="<?php echo $aksi?>?module=pelanggan&aksi=edit" role="form" method="post">             

  <div class="form-group">
    <label class="col-sm-4 control-label">ID Pelanggan </label>
    <div class="col-sm-5">
      <input type="text" class="form-control" readonly name="id_pelanggan" value="<?php echo $edit['id_pelanggan']; ?>" >
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-4 control-label">Nama Pelanggan</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" required="required" name="nama_pelanggan"value="<?php echo $edit['nama_pelanggan']; ?>" onkeypress="return goodchar(event,'',this)">
    </div>
  </div>

   <div class="form-group">
    <label class="col-sm-4 control-label">Email</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" required="required" name="email"value="<?php echo $edit['email']; ?>">
    </div>
  </div>

   <div class="form-group">
    <label class="col-sm-4 control-label">No Telphone</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" required="required" name="notelp"value="<?php echo $edit['notelp']; ?>"onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-4 control-label">Alamat</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" required="required" name="alamat"value="<?php echo $edit['alamat']; ?>" onkeypress="return goodall(event,'',this)">
    </div>
  </div>

<div class="form-group">
    <label class="col-sm-4"></label>
    <div class="col-sm-5">
  <hr/>
<button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<a href="?module=pelanggan">
<button class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Batal</button></a>
    </div>
</div>
</form>
</div>
</div>
<!----- ------------------------- END EDIT DATA MASTER pelanggan ------------------------- ----->
<?php
break;
}
?>
