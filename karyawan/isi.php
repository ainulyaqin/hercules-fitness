<?php
include "include/koneksi.php";

if ($_GET['module'] == "home") {
	include "module/home.php";
}
else if ($_GET['module'] == "pelanggan") {
	include "module/pelanggan/pelanggan.php";	
}
else if ($_GET['module'] == "user") {
	include "module/user/user.php";	
}
else if ($_GET['module'] == "penjualan") {
	include "module/penjualan/penjualan.php";	
}
else if ($_GET['module'] == "detail") {
	include "module/detail/detail.php";	
}
else if ($_GET['module'] == "edit_user") {
	include "module/edit_user.php";
}
else if ($_GET['module'] == "riwayat") {
	include "module/detail/riwayat.php";
}
else if ($_GET['module'] == "program") {
	include "module/program/program.php";
}
else if ($_GET['module'] == "detailprogram") {
	include "module/program/detailprogram.php";
}
else if ($_GET['module'] == "riwayatprogram") {
	include "module/program/riwayatprogram.php";
}
else if ($_GET['module'] == "riwayatprogram_aksi") {
	include "module/program/riwayatprogram_aksi.php";
}
else if ($_GET['module'] == "nota") {
	include "module/detail/nota.php";
}
?>