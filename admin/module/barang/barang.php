<?php
$aksi="module/barang/barang_aksi.php";
switch($_GET[aksi]){
default:
?>
<!----- ------------------------- MENAMPILKAN DATA MASTER Barang ------------------------- ----->			
<center> <div class="batas"> </div></center>
<hr/>
	<div class="box box-solid box-danger" style="font-family: Geneva; border-radius: 15px;">
		<div class="box-header" style="border-radius: 15px;">
		<h3 class="btn btn disabled box-title" style="font-family: Geneva; border-radius: 15px;">
		<i class="fa fa-folder-o"></i>
		Data Master Barang </h3>
		<a class="btn btn-default pull-right"href="?module=barang&aksi=tambah" style="border-radius: 15px;">
		<i class="fa fa-plus"></i> Tambah Data</a>	
		</div>		
	<div class="box-body">
	<table id="example1" class="table table-bordered table-striped">
<thead>
	<tr class="text-red">
		<th class="col-sm-1">No</th>		
		<th class="col-sm-2">Nama Barang</th>
		<th class="col-sm-1">Harga</th> 
		<th class="col-sm-1">Stok</th>
    <th class="col-sm-1">Kegunaan</th> 
		<th class="col-sm-1">Aksi</th> 
	</tr>
</thead>

<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT * FROM tb_barang";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 

$Kode = $tampilkan['id_barang'];
?>
	<tr>
	<td><?php echo $no++; ?></td>	
	<td><?php echo $tampilkan['nama_barang']; ?></td>
	<td>Rp.<?php echo number_format($tampilkan['harga']) ?>,-</td>
	<td><?php echo $tampilkan['stok']; ?></td>
  <td><?php echo $tampilkan['kegunaan']; ?></td>
	<td align="center">
	<a class="btn btn-xs btn-info" href="?module=barang&aksi=edit&id_barang=<?php echo $tampilkan['id_barang'];?>" alt="Edit Data"><i class="glyphicon glyphicon-pencil"></i></a>
	<a class="btn btn-xs btn-danger"href="<?php echo $aksi ?>?module=barang&aksi=hapus&id_barang=<?php echo $tampilkan['id_barang'];?>"  alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA <?php echo $Kode; ?>	?')"> <i class="glyphicon glyphicon-trash"></i></a>
	</td>
	
	<?php
	}
	?>
	</tr>
			</tbody>
		</table>
	</div><!-- /.box-body -->
</div><!-- /.box -->
<!----- ------------------------- END MENAMPILKAN DATA MASTER barang ------------------------- ----->
<?php 
break;
 case "tambah": 
//ID
$sql ="SELECT max(id_barang) as terakhir from tb_barang";
  $hasil = mysql_query($sql);
  $data = mysql_fetch_array($hasil);
  $lastID = $data['terakhir'];
  $lastNoUrut = substr($lastID, 3, 9);
  $nextNoUrut = $lastNoUrut + 1;
  $nextID = "BRG".sprintf("%03s",$nextNoUrut);
?>
<!----- ------------------------- TAMBAH DATA MASTER barang ------------------------- ----->
<h3 class="box-title margin text-center">Tambah Data Master Barang</h3>
<center> <div class="batas"> </div></center>
<hr/>

<form class="form-horizontal" action="<?php echo $aksi?>?module=barang&aksi=tambah" role="form" method="post">             
  <div class="form-group">
    <label class="col-sm-2 control-label">ID Barang </label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" readonly name="id_barang" value="<?php echo  $nextID; ?>" >
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-2 control-label">Detail Kategori</label>
    <div class="col-sm-7">
		<select name="id_detailkategori" class="form-control">
		<option value=" "> -- Pilih Kategori -- </option>
		<?php $q = mysql_query ("select * from tb_detailkategori");
		while ($k = mysql_fetch_array($q)){ ?>
		<option value="<?php echo $k['id_detailkategori']; ?>" 
		<?php (@$h['id_detailkategori']==$k['id_detailkategori'])?print(" "):print(""); ?>	> <?php echo $k['id_detailkategori']; echo " / ".$k['nama_detailkategori']; ?>
		</option> <?php	} ?>
		</select>
  </div></div>  
    
  <div class="form-group">
    <label class="col-sm-2 control-label">Nama Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="nama_barang" placeholder="Nama Barang" onkeypress="return goodchar(event,'',this)">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Harga Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="harga" placeholder="Harga Barang" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>
<div class="form-group">
    <label class="col-sm-2 control-label">Stok Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="stok" placeholder="Stok Barang" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>  
<div class="form-group">
    <label class="col-sm-2 control-label">Kegunaan</label>
    <div class="col-sm-7">
     <select class="form-control" name="kegunaan">
             <option value="Cutting">Cutting</option>
             <option value="Bulking">Bulking</option>
     </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-4 control-label">  </label>
    <div class="col-sm-5">
<button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i><i> Reset</i></button>
    </div>
  </div> 
</form> 
<!----- ------------------------- END TAMBAH DATA MASTER barang ------------------------- ----->
<?php	
break;
case "edit" :
$data=mysql_query("select * from tb_barang where id_barang='$_GET[id_barang]'");
$edit=mysql_fetch_array($data);
?>
<!----- ------------------------- EDIT DATA MASTER barang ------------------------- ----->
<h3 class="box-title margin text-center">Edit Data Barang "<?php echo $_GET['id_barang']; ?>"</h3>
<br/>
<form class="form-horizontal" action="<?php echo $aksi?>?module=barang&aksi=edit" role="form" method="post">             
	<div class="form-group">
    <label class="col-sm-2 control-label">ID Barang </label>
    <div class="col-sm-7">
      <input type="text" class="form-control" readonly name="id_barang" value="<?php echo $edit['id_barang']; ?>" >
    </div>
  </div>
   <div class="form-group">
    <label class="col-sm-2 control-label">Detail Kategori</label>
    <div class="col-sm-7">
		<select name="id_detailkategori" class="form-control">
		<option value=" "> -- Pilih Detail Kategori -- </option>
		<?php $q = mysql_query ("select * from tb_detailkategori");
		while ($k = mysql_fetch_array($q)){ ?>
		<option value="<?php echo $k['id_detailkategori']; ?>" 
		<?php (@$h['id_detailkategori']==$k['id_detailkategori'])?print(" "):print(""); ?>	> <?php echo $k['id_detailkategori']; echo " / ".$k['nama_detailkategori']; ?>
		</option> <?php	} ?>
		</select>
  </div></div> 

   <div class="form-group">
    <label class="col-sm-2 control-label">Nama Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="nama_barang"value="<?php echo $edit['nama_barang']; ?>" onkeypress="return goodchar(event,'',this)">
    </div>
  </div>
   
  <div class="form-group">
    <label class="col-sm-2 control-label">Harga Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="harga"value="<?php echo $edit['harga']; ?>" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Stok Barang</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="stok"value="<?php echo $edit['stok']; ?>" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Kegunaan</label>
    <div class="col-sm-7">
     <select class="form-control" name="kegunaan">
             <option value="Cutting">Cutting</option>
             <option value="Bulking">Bulking</option>
     </select>
    </div>
  
<div class="form-group">
    <label class="col-sm-4"></label>
    <div class="col-sm-5">
	<hr/>
<button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<a href="?module=barang">
<button class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Batal</button></a>
    </div>
</div>

</form>
</div>
</div>
<!----- ------------------------- END EDIT DATA MASTER lokasi ------------------------- ----->
<?php
break;
}
?>