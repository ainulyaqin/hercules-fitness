<?php
$aksi="module/program/program_aksi.php";
switch($_GET[aksi]){
default:
?>
<!----- ------------------------- MENAMPILKAN DATA MASTER program ------------------------- ----->			
<center> <div class="batas"> </div></center>
<br/>
<div class="row">
<div class="col-md-12" style="font-family: Geneva;">
	<div class="box box-solid box-danger" style="border-radius: 15px;">
		<div class="box-header" style="border-radius: 15px;">
		<h3 class="btn btn disabled box-title">
		<i class="fa fa-plus"></i>
		Tambah Data Program</h3>		 	
		</div>		
	<div class="box-body">
	<?php
$sql ="SELECT max(id_program) as terakhir from tb_program";
  $hasil = mysql_query($sql);
  $data = mysql_fetch_array($hasil);
  $lastID = $data['terakhir'];
  $lastNoUrut = substr($lastID, 3, 9);
  $nextNoUrut = $lastNoUrut + 1;
  $nextID = "PRG".sprintf("%03s",$nextNoUrut);
?> 
	 <form class="form-horizontal" action="<?php echo $aksi?>?module=program&aksi=tambah" role="form" method="post">             

  <div class="form-group">
    <label class="col-sm-2 control-label">ID Program</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" readonly name="id_program" value="<?php echo  $nextID; ?>" >
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">ID Pelanggan</label>
    <div class="col-sm-7">
      <input type="text" class="search" required="required" name="id_pelanggan" style="width: 100%; padding: 4px;" placeholder="Silahkan Masukan Id Pelanggan">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Jenis Kelamin</label>
    <div class="col-sm-7">
     <select class="form-control" name="jk">
      <option value=" "> -- Pilih Jenis Kelamin -- </option>
             <option value="Pria">Pria</option>
             <option value="Wanita">Wanita</option>
     </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Usia Pelanggan</label>
    <div class="col-sm-7">
      <input type="number" min="15" max="50" class="form-control" required="required" name="usia" placeholder="Usia Pelanggan" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Berat Badan Sekarang (Kg)</label>
    <div class="col-sm-7">
      <input type="number" min="20"  class="form-control" required="required" name="bbskrg" placeholder="Berat Badan Sekarang" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Berat Badan Target (Kg)</label>
    <div class="col-sm-7">
      <input type="number" max="200" class="form-control" required="required" name="bbtarget" placeholder="Berat Badan Yang Di inginkan" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Tgl Mulai</label>
    <div>
  <div class="col-sm-7">
    <div class="input-group">
  <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
    </div>
      <input type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" max="" required="required" name="tgl_masuk">
  </div></div></div></div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Tgl Selesai</label>
    <div>
  <div class="col-sm-7">
    <div class="input-group">
  <div class="input-group-addon">
            <i class="fa fa-calendar"> </i>
    </div>
      <input type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" max="" required="required" name="tgl_selesai">
  </div></div></div></div>

  <div class="form-group">
    <label class="col-sm-4"></label>
    <div class="col-sm-4">
	<hr/>
      <button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
<button type="reset" class="btn btn-danger"><i class="fa fa-refresh"></i><i> Reset</i></button> 
    </div>
  </div>
</form>
	</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
<div class="col-md-12" style="font-family: Geneva;">
	<div class="box box-solid box-danger" style="border-radius: 15px;">
		<div class="box-header" style="border-radius: 15px;">
		<h3 class="btn disabled box-title">
		<i class="fa fa-male"></i>
		Data Program Latihan</h3>	
		</div>		
	<div class="box-body">
	<table id="example2" class="table table-bordered table-striped">
<thead>
	<tr class="text-red">
		<th class="col-sm-1">No</th> 
		<th class="col-sm-2">Nama Pelanggan</th>
    <th class="col-sm-1">Jenis Kelamin</th>
    <th class="col-sm-1">Usia Pelanggan</th>
    <th class="col-sm-2">BB Sekarang (Kg)</th>
    <th class="col-sm-2">BB Target (Kg)</th>
    <th class="col-sm-2">Jenis Latihan</th>
    <th class="col-sm-2">Tanggal Latihan Awal</th>
    <th class="col-sm-2">Tanggal Latihan Akhir</th>
    <th class="col-sm-2">Total Bulan</th>
    <th class="col-sm-2">AKSI</th> 	
	</tr>
</thead>
<tbody>
<?php 
// Tampilkan data dari Database
$sql = "SELECT tb_pelanggan.`nama_pelanggan`, tb_program.`id_program`, jk,usia, bbskrg, bbtarget, jenis_latihan,`tgl_masuk`,tgl_selesai,totalbulan FROM tb_program
 JOIN tb_pelanggan ON tb_program.`id_pelanggan` = tb_pelanggan.id_pelanggan
 -- JOIN tb_detailprogram ON tb_detailprogram.`id_program` = tb_program.`id_program`
";
$tampil = mysql_query($sql);
$no=1;
while ($tampilkan = mysql_fetch_array($tampil)) { 
$Kode = $tampilkan['id_program'];
$jenis_latihan = $tampil ['jenis_latihan']; 


?>

	<tr>
	<td><?php echo $no++; ?></td> 
	<td><?php echo $tampilkan['nama_pelanggan']; ?></td>
  <td><?php echo $tampilkan['jk']; ?></td>
  <td><?php echo $tampilkan['usia']; ?></td>
  <td><?php echo $tampilkan['bbskrg']; ?></td>
  <td><?php echo $tampilkan['bbtarget']; ?></td>
  <td><?php echo $tampilkan['jenis_latihan']; ?></td>
  <td><?php echo $tampilkan['tgl_masuk']; ?></td>
  <td><?php echo $tampilkan['tgl_selesai']; ?></td>
	<td><?php echo $tampilkan['totalbulan']; ?></td>
	<td align="center">  
  <a class="btn btn-xs btn-info"href="?module=riwayatprogram&id_program=<?php echo $tampilkan['id_program'];?>"> <i class="fa fa-search-plus"></i></a> 
  <a class="btn btn-xs btn-info" href="?module=program&aksi=edit&id_program=<?php echo $tampilkan['id_program'];?>" alt="Edit Data"><i class="glyphicon glyphicon-pencil"></i></a> 
	<a class="btn btn-xs btn-danger"href="<?php echo $aksi ?>?module=program&aksi=hapus&id_program=<?php echo $tampilkan['id_program'];?>" alt="Delete Data" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA ?')"> <i class="glyphicon glyphicon-trash"></i></a>
	</td>
	<?php
	}
	?>
	</tr>
			</tbody>
		</table>
	</div><!-- /.box-body -->
</div><!-- /.box -->
</div>
<!----- ------------------------- END TAMBAH DATA MASTER Program ------------------------- ----->
<?php	
break;
case "edit" :
$data=mysql_query("SELECT * FROM tb_program WHERE `id_program`='$_GET[id_program]'");
$edit=mysql_fetch_array($data);
?>

<!----- ------------------------- EDIT DATA MASTER Program ------------------------- ----->
<h3 class="box-title margin text-center">Edit Data Program Latihan "<?php echo $_GET['id_program']; ?>"</h3>
<br/>
<form class="form-horizontal" action="<?php echo $aksi?>?module=program&aksi=edit" role="form" method="post">             

  <div class="form-group">
    <label class="col-sm-2 control-label">ID Program </label>
    <div class="col-sm-7">
      <input type="text" class="form-control" readonly name="id_program" value="<?php echo $edit['id_program']; ?>" >
    </div>
  </div>


  <div class="form-group">
    <label class="col-sm-2 control-label">ID Pelanggan</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" readonly name="id_pelanggan" value="<?php echo $edit['id_pelanggan']; ?> " >
    </div>
  </div>


   <div class="form-group">
    <label class="col-sm-2 control-label">Jenis Kelamin</label>
    <div class="col-sm-7">
     <select class="form-control" name="jk" value="">
             <option value="Pria">Pria</option>
             <option value="Wanita">Wanita</option>
     </select>
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Usia Pelanggan</label>
    <div class="col-sm-7">
      <input type="number" min="15" max="50" class="form-control" required="required" value="<?php echo $edit['usia']; ?>" name="usia" placeholder="Usia Pelanggan" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Berat Badan Sekarang (Kg)</label>
    <div class="col-sm-7">
      <input type="number" min="20" class="form-control" required="required" name="bbskrg" value="<?php echo $edit['bbskrg']; ?>" placeholder="Berat Badan Sekarang" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Berat Badan Target (Kg)</label>
    <div class="col-sm-7">
      <input type="number" max="200" class="form-control" required="required" name="bbtarget" value="<?php echo $edit['bbtarget']; ?>" placeholder="Berat Badan Yang Di inginkan" onkeypress="return goodnumber(event,'',this)">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Jenis Latihan</label>
    <div class="col-sm-7">
     <select class="form-control" name="jenis_latihan" value="<?php echo $edit['jenis_latihan'];?>">
             <option value="Cutting">Cutting</option>
             <option value="Bulking">Bulking</option>
     </select>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Tgl Mulai</label>
    <div>
  <div class="col-sm-7">
    <div class="input-group">
  <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
    </div>
      <input type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required="required" name="tgl_masuk">
  </div></div></div></div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Tgl Selesai</label>
    <div>
  <div class="col-sm-7">
    <div class="input-group">
  <div class="input-group-addon">
            <i class="fa fa-calendar"> </i>
    </div>
      <input type="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required="required" name="tgl_selesai">
  </div></div></div></div>

  <div class="form-group">
    <label class="col-sm-2 control-label">Total Bulan</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required="required" name="totalbulan" value="<?php echo $edit['totalbulan'];?>" placeholder="Total Lathan">
    </div>
<div class="form-group">
    <label class="col-sm-4"></label>
    <div class="col-sm-5">
	<hr/>
<button type="submit"name="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
<a href="?module=pelanggan">
<button class="btn btn-warning"><i class="glyphicon glyphicon-remove"></i> Batal</button></a>
    </div>
</div>
</form>
</div>
</div>
<!----- ------------------------- END EDIT DATA MASTER pelanggan ------------------------- ----->
<?php
break;
}
?>
