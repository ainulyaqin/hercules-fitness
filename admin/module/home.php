<?php
include ("../inc/koneksi.php"); 
include ("../inc/fungsi_hdt");  ?>
<br/>
<div style="margin-right:10%;margin-left:15%;border-radius: 15px;" class="alert alert-danger alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<p><i class="icon fa fa-info"></i>
Welcome <?php echo $_SESSION['nama']; ?>! &nbsp;&nbsp;
Anda berada di halaman "<?php echo $_SESSION['level']; ?>"
</p>
</div> 
<div class="box box-solid box-danger" style="border-radius: 15px;">
<div class="box-header" style="border-radius: 15px;">
<i class="fa fa-info"></i>Harap Di Baca & Di Pahami !
</div>
<div class="box-body">
<h4>Hak Akses sebagai Admin:</h4>
<li>Mengelola data User</li>
<li>Mengelola data master Pegawai</li>
<li>Mengelola data master Pelanggan</li>
<li>Mengelola data master Kategori</li>
<li>Mengelola data master Promo</li>
<li>Mengelola data master Barang</li>
<li>Mengelola data transaksi Program Latihan</li>
<li>Mengelola data transaksi Penjualan</li>
</div>
</div><!-- /.row -->

 